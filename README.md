# qi3pc

[![pipeline status](https://gitlab.com/hantz/qi3pc/badges/master/pipeline.svg)](https://gitlab.com/hantz/qi3pc/commits/master)

Qt based library to communicate with i3wm via its IPC interface.

I couldn't find a C++ and/or Qt library that maps cleanly enough to the i3wm IPC
interface to use, so I made my own.

## Workflow

The messages, replies and events that i3wm IPC API provides are
accessible through methods and signals with obvious names. Getting information from i3 is as simple as sending a message and waiting for a
reply or subscribing to events and waiting for them to be triggered.
This is done with the signal/slot mechanisms offered
by Qt and normal C++ functions.

In addition to the method mentioned above, some information like workspaces, tree, config, etc. is cached for synchronous retrieval. This
type  of data is accessible in the form of a pair consisting of a QJsonObject or a QJsonArray and a 64 bit integer holding the last time it has been updated.

### Message and replies

The method to work with messages/replies consist of three steps: sending a message, then waiting for the notification of reply and finally read the reply.

Say we want to get the list of workspaces from i3 using this method, the could would look like the following:

```cpp

YourClass() {
ipc.connect();

// ipc below is of type qi3pc
// setup for step 2
QObject::connect(&ipc, &qi3pc::workspacesUpdated, this, &YourClass::handleWorkspaceChanges);

// step 1
ipc.fetchWorkspaces();

// ...
}

void
YourClass::handleWorkspaceChanges(const qi3pc::DataArray& data)
{
  qDebug() << "List of workspaces";
	qDebug() << data->first;

  // step 3
	// this is optional since the data is made available in the
	// notification (the Qt signal)
	qDebug() << "Cached list of workspaces";
	qDebug() << ipc.workspaces().first();
}
```
### Subscription and events

When working with subscription/events you need to first subscribe to the
types of events that interest you, then wait for the event to be triggered by i3 and finally consume the associated data. Say we want to know about window changes, we would proceed as follows:

```cpp
YourClass() {
  // ipc is of type qi3pc
	ipc.connect();

  // setup for step 2
	QObject::connect(&ipc, qi3pc::windowEvent, this, handleWindowEvents);

  // step 1
	QStringList events("window");
	ipc.subscribe(events);

  //...
}

void
YourClass::handleWindowEvents(qi3pc::WindowChange change, const QJsonObject& container)
{
  Q_UNUSED(change)

  qDebug() << "Parent of changed window";
	qDebug() << container;
}
```

Notice that the data made available through the event signals are
typically not cached and can't be retrieved if you don't save them
yourself.

## Other examples
See the [i3 module](https://gitlab.com/hantz/buffalo/blob/4eb88adb938eaff71ed083754a449e7867f3ed2b/src/cpp/modules/i3.cpp)
of the WIP [buffalo bar](https://gitlab.com/hantz/buffalo) for some
ideas on how to use this.

[qi3pc on GitLab](https://gitlab.com/hantz/qi3pc)
