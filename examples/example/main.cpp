#include <QCoreApplication>
#include <qi3pc/qi3pc.h>

int
main(int argc, char* argv[])
{
  QCoreApplication a(argc, argv);

  qi3pc qi3;
  return a.exec();
}
