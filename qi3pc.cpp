#include "qi3pc.h"

qi3pc::qi3pc(QObject* parent)
  : QObject(parent)
{
  m_socketPath = socketPath();

  QObject::connect(
    &m_eventSocket, &QLocalSocket::readyRead, this, &qi3pc::processEvent);
  QObject::connect(
    &m_messageSocket, &QLocalSocket::readyRead, this, &qi3pc::processReply);

  // connect the fetch signals to the appropriate behaviour
  // when a fetch signal is received the appropriate message is sent to the i3
  // socket when the reply come back a signal is triggered with the reply's data

  QObject::connect(
    this, &qi3pc::fetchWorkspaces, [this]() { sendMessage(IpcType::Workspaces); });

  QObject::connect(
    this, &qi3pc::fetchTree, [this]() { sendMessage(IpcType::Tree); });

  QObject::connect(
    this, &qi3pc::fetchOutputs, [this]() { sendMessage(IpcType::Outputs); });

  QObject::connect(
    this, &qi3pc::fetchMarks, [this]() { sendMessage(IpcType::Marks); });

  QObject::connect(
    this, &qi3pc::fetchConfig, [this]() { sendMessage(IpcType::Config); });

  QObject::connect(this, &qi3pc::fetchBarConfig,
                   [this](const QString& id) {
                     QByteArray payload;
		      payload.append(id.toStdString().c_str());
                     sendMessage(IpcType::Config, payload);
                   });

  QObject::connect(this, &qi3pc::fetchBarConfigs, [this]() {
    sendMessage(IpcType::BarConfig);
  });

  QObject::connect(
    this, &qi3pc::fetchVersion, [this]() { sendMessage(IpcType::Version); });

  QObject::connect(
    this, &qi3pc::fetchModes, [this]() { sendMessage(IpcType::BindingModes); });
}

qi3pc::~qi3pc()
{
  m_eventSocket.close();
  m_messageSocket.close();

  m_eventSocket.abort();
  m_messageSocket.abort();
}

void
qi3pc::processEvent()
{
  Message m = processMessage(m_eventSocket);
  if (!m) {
    return;
  }

  auto [data, type] = m.value();

  if (type & (1u << 31)) {
    switch (static_cast<IpcEvent>(type)) {
      case IpcEvent::Workspace:
        processWorkspaceEvent(data);
        break;
      case IpcEvent::Output:
        processOutputEvent(data);
        break;
      case IpcEvent::Mode:
        processModeEvent(data);
        break;
      case IpcEvent::Window:
        processWindowEvent(data);
        break;
      case IpcEvent::BarUpdate:
        processBarUpdateEvent(data);
        break;
      case IpcEvent::Binding:
        processBindingEvent(data);
        break;
      case IpcEvent::Shutdown:
        processShutdownEvent(data);
        break;
      case IpcEvent::Tick:
        processTickEvent(data);
        break;
    }
  } else {
    switch (static_cast<IpcType>(type)) {
      case IpcType::Subscribe:
        emit subscribed(data["success"].toBool());
        break;
      default:
        break;
    }
  }
}

void
qi3pc::processReply()
{
  Message m = processMessage(m_messageSocket);
  if (!m) {
    return;
  }

  auto [data, type] = m.value();

  switch (static_cast<IpcType>(type)) {
    case IpcType::Command:
      processCommandReply(data);
      break;
    case IpcType::Workspaces:
      processWorkspaceReply(data);
      break;
    case IpcType::Subscribe:
      // we should never get there
      break;
    case IpcType::Outputs:
      processOutputReply(data);
      break;
    case IpcType::Tree:
      processTreeReply(data);
      break;
    case IpcType::Marks:
      processMarkReply(data);
      break;
    case IpcType::BarConfig:
      processBarConfigReply(data);
      break;
    case IpcType::Version:
      processVersionReply(data);
      break;
    case IpcType::BindingModes:
      processModeReply(data);
      break;
    case IpcType::Config:
      processConfigReply(data);
      break;
    case IpcType::Tick:
      processTickReply(data);
      break;
    case IpcType::Sync:
      processSyncReply(data);
      break;
  }
}

void
qi3pc::processCommandReply(const QJsonDocument& doc)
{
  Error error = {};
  if (auto val = doc["error"]; val != QJsonValue::Undefined) {
    error = val.toString();
  }

  emit commandRan(doc["success"].toBool(), error);
}

void
qi3pc::processWorkspaceReply(const QJsonDocument& doc)
{
  m_workspaces = std::make_pair(doc.array(), QDateTime::currentMSecsSinceEpoch());
  emit workspacesUpdated(m_workspaces);
}

void
qi3pc::processOutputReply(const QJsonDocument& doc)
{
  m_outputs->first = doc.array();
  m_outputs->second = QDateTime::currentMSecsSinceEpoch();
  emit outputsUpdated(m_outputs);
}

void
qi3pc::processTreeReply(const QJsonDocument& doc)
{
  m_tree = std::make_pair(doc.object(), QDateTime::currentMSecsSinceEpoch());
  emit treeUpdated(m_tree);
}

void
qi3pc::processMarkReply(const QJsonDocument& doc)
{
  m_marks->first = doc.array();
  m_marks->second = QDateTime::currentMSecsSinceEpoch();
  emit marksUpdated(m_marks);
}

void
qi3pc::processBarConfigReply(const QJsonDocument& doc)
{
  if (doc.isArray()) {
    for (auto id : doc.array()) {
      if (auto str = id.toString();
          m_barConfigs->first[str] == QJsonValue::Undefined) {
        m_barConfigs->first[str] = QJsonObject();
        emit newBarConfig(str);
      }
    }
  } else {
    auto id = doc["id"].toString();
    auto config = doc.object();
    m_barConfigs->first[id] = config;
    m_barConfigs->second = QDateTime::currentMSecsSinceEpoch();
    emit barConfigUpdated(config);
  }
}

void
qi3pc::processVersionReply(const QJsonDocument& doc)
{
  m_version->first = doc.object();
  m_version->second = QDateTime::currentMSecsSinceEpoch();
  emit versionUpdated(m_version);
}

void
qi3pc::processModeReply(const QJsonDocument& doc)
{
  m_modes->first = doc.array();
  m_modes->second = QDateTime::currentMSecsSinceEpoch();
  emit modesUpdated(m_modes);
}

void
qi3pc::processConfigReply(const QJsonDocument& doc)
{
  m_config->first = doc["config"].toObject();
  m_config->second = QDateTime::currentMSecsSinceEpoch();
  emit configUpdated(m_config);
}

void
qi3pc::processTickReply(const QJsonDocument& doc)
{
  emit tickSent(doc["success"].toBool());
}

void
qi3pc::processSyncReply(const QJsonDocument& doc)
{
  emit synced(doc["success"].toBool());
}

void
qi3pc::processWorkspaceEvent(const QJsonDocument& doc)
{
  auto change = workspaceChangeFromString(doc["change"].toString());

  if (change == WorkspaceChange::Unknown) {
    return;
  }

  auto old = doc["old"].toObject();
  auto current = doc["current"].toObject();
  emit workspaceEvent(change, current, old);
}

void
qi3pc::processOutputEvent(const QJsonDocument& data)
{
  auto change = outputChangeFromString(data["change"].toString());

  if (change == OutputChange::Unknown) {
    return;
  }

  emit outputEvent(change);
}

void
qi3pc::processModeEvent(const QJsonDocument& doc)
{
  emit modeEvent(doc["change"].toString(), doc["pango_markup"].toBool());
}

void
qi3pc::processWindowEvent(const QJsonDocument& doc)
{
  auto change = windowChangeFromString(doc["change"].toString());

  if (change == WindowChange::Unknown) {
    return;
  }

  emit windowEvent(change, doc["container"].toObject());
}

void
qi3pc::processBarUpdateEvent(const QJsonDocument& doc)
{
  emit barUpdateEvent(doc.object());
}

void
qi3pc::processBindingEvent(const QJsonDocument& doc)
{
  auto change = bindingChangeFromString(doc["change"].toString());

  if (change == BindingChange::Unknown) {
    return;
  }

  emit bindingEvent(change, doc["binding"].toObject());
}

void
qi3pc::processShutdownEvent(const QJsonDocument& doc)
{
  auto change = shutdownChangeFromString(doc["change"].toString());

  if (change == ShutdownChange::Unknown) {
    return;
  }

  emit shutdownEvent(change);
}

void
qi3pc::processTickEvent(const QJsonDocument& doc)
{
  emit tickEvent(doc["first"].toBool(), doc["payload"].toObject());
}

qi3pc::WorkspaceChange
qi3pc::workspaceChangeFromString(const QString& s) const
{
  if (s == "focus") {
    return WorkspaceChange::Focus;
  } else if (s == "init") {
    return WorkspaceChange::Init;
  } else if (s == "empty") {
    return WorkspaceChange::Empty;
  } else if (s == "urgent") {
    return WorkspaceChange::Urgent;
  } else if (s == "reload") {
    return WorkspaceChange::Reload;
  } else if (s == "rename") {
    return WorkspaceChange::Rename;
  } else if (s == "restored") {
    return WorkspaceChange::Restored;
  } else if (s == "move") {
    return WorkspaceChange::Move;
  } else {
    return WorkspaceChange::Unknown;
  }
}

qi3pc::WindowChange
qi3pc::windowChangeFromString(const QString& s) const
{
  if (s == "new") {
    return WindowChange::New;
  } else if (s == "close") {
    return WindowChange::Close;
  } else if (s == "focus") {
    return WindowChange::Focus;
  } else if (s == "title") {
    return WindowChange::Title;
  } else if (s == "fullscreen_mode") {
    return WindowChange::Fullscreen;
  } else if (s == "move") {
    return WindowChange::Move;
  } else if (s == "floating") {
    return WindowChange::Floating;
  } else if (s == "urgent") {
    return WindowChange::Urgent;
  } else if (s == "mark") {
    return WindowChange::Mark;
  } else {
    return WindowChange::Unknown;
  }
}

qi3pc::ShutdownChange
qi3pc::shutdownChangeFromString(const QString& s) const
{
  if (s == "restart") {
    return ShutdownChange::Restart;
  } else if (s == "exit") {
    return ShutdownChange::Exit;
  } else {
    return ShutdownChange::Unknown;
  }
}

qi3pc::OutputChange
qi3pc::outputChangeFromString(const QString& s) const
{
  if (s == "unspecified") {
    return OutputChange::Unspecified;
  } else {
    return OutputChange::Unknown;
  }
}

qi3pc::BindingChange
qi3pc::bindingChangeFromString(const QString& s) const
{
  if (s == "run") {
    return BindingChange::Run;
  } else {
    return BindingChange::Unknown;
  }
}

qi3pc::Message
qi3pc::processMessage(QLocalSocket& socket)
{
  char c[IpcMagicLength];
  socket.read(c, IpcMagicLength);
  if (strncmp(IpcMagicString, c, strlen(IpcMagicString)) != 0) {
    return {};
  }

  quint32 size;
  if (auto read_size = socket.read(reinterpret_cast<char*>(&size), sizeof size);
      read_size != sizeof size) {
    return {};
  }

  quint32 type;
  if (auto read_size = socket.read(reinterpret_cast<char*>(&type), sizeof type);
      read_size != sizeof type) {
    return {};
  }

  QJsonDocument data = QJsonDocument::fromJson(socket.read(size));

  if (socket.bytesAvailable() > 0) {
		emit socket.readyRead();
  }

  return std::make_pair(data, type);
}

QString
qi3pc::socketPath() const
{

  if (auto path = QProcessEnvironment::systemEnvironment().value("I3SOCK");
      !path.isEmpty()) {
    return path;
  }

  // TODO: try to get socket path from root window property

  QProcess* process = new QProcess();
  process->start("i3", QStringList("--get-socketpath"));
  process->waitForReadyRead();
  auto path = QString(process->readAllStandardOutput()).trimmed();
  process->kill();
  return path;
}

qi3pc::DataArray
qi3pc::workspaces() const
{
  return m_workspaces;
}

qi3pc::DataObject
qi3pc::tree() const
{
  return m_tree;
}

qi3pc::DataArray
qi3pc::outputs() const
{
  return m_outputs;
}

qi3pc::DataArray
qi3pc::marks() const
{
  return m_marks;
}

qi3pc::DataObject
qi3pc::barConfigs() const
{
  return m_barConfigs;
}

qi3pc::DataObject
qi3pc::version() const
{
  return m_version;
}
qi3pc::DataArray
qi3pc::modes() const
{
  return m_modes;
}

qi3pc::DataObject
qi3pc::config() const
{
  return m_config;
}

void
qi3pc::sendMessage(IpcType type, const QByteArray& payload)
{
  m_messageSocket.flush();
  qint32 size = payload.size();
  m_messageSocket.write(qi3pc::IpcMagicString, strlen(qi3pc::IpcMagicString));
  m_messageSocket.write(reinterpret_cast<const char*>(&size), sizeof size);
  m_messageSocket.write(reinterpret_cast<const char*>(&type), sizeof type);

  if (size > 0) {
    m_messageSocket.write(payload.data(), size);
  }
}

bool
qi3pc::connect()
{
  if (m_messageSocket.state() == QLocalSocket::ConnectedState &&
      m_eventSocket.state() == QLocalSocket::ConnectedState) {
    return true;
  }

  m_messageSocket.connectToServer(m_socketPath);
  m_eventSocket.connectToServer(m_socketPath);

  m_messageSocket.waitForConnected();
  m_eventSocket.waitForConnected();

  return m_messageSocket.state() == QLocalSocket::ConnectedState &&
         m_eventSocket.state() == QLocalSocket::ConnectedState;
}

bool
qi3pc::disconnect()
{
  m_messageSocket.disconnectFromServer();
  m_eventSocket.disconnectFromServer();

  m_messageSocket.waitForDisconnected();
  m_eventSocket.disconnectFromServer();

  return (m_messageSocket.state() != QLocalSocket::ConnectedState &&
          m_messageSocket.state() != QLocalSocket::ConnectingState) &&
         (m_eventSocket.state() != QLocalSocket::ConnectedState &&
          m_eventSocket.state() != QLocalSocket::ConnectingState);
}

bool
qi3pc::connected()
{
  if (m_messageSocket.state() == QLocalSocket::ConnectingState ||
      m_eventSocket.state() == QLocalSocket::ConnectingState) {
    m_messageSocket.waitForConnected();
    m_eventSocket.waitForConnected();
  }

  return m_messageSocket.state() == QLocalSocket::ConnectedState &&
         m_eventSocket.state() == QLocalSocket::ConnectedState;
}

void
qi3pc::subscribe(const QStringList& events)
{
  QJsonDocument doc(QJsonArray::fromStringList(events));
  QByteArray payload = doc.toJson();
  qint32 size = payload.size();
  static IpcType type = IpcType::Subscribe;

  m_eventSocket.write(qi3pc::IpcMagicString, strlen(qi3pc::IpcMagicString));
  m_eventSocket.write(reinterpret_cast<const char*>(&size), sizeof size);
  m_eventSocket.write(reinterpret_cast<const char*>(&type),
                      sizeof IpcType::Subscribe);

  if (size > 0) {
    m_eventSocket.write(payload.data(), size);
  }
}
