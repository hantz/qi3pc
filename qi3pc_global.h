#ifndef QI3PC_GLOBAL_H
#define QI3PC_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(QI3PC_LIBRARY)
#  define QI3PCSHARED_EXPORT Q_DECL_EXPORT
#else
#  define QI3PCSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // QI3PC_GLOBAL_H
