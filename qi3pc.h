#ifndef QI3PC_H
#define QI3PC_H

#include "QtNetwork/qlocalsocket.h"
#include "qi3pc_global.h"
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLocalSocket>
#include <QMutexLocker>
#include <QProcessEnvironment>
#include <QTime>
#include <i3/ipc.h>
#include <optional>

/*!
 * \class qi3pc
 * \brief Class mapping to the to the i3wm IPC interface.
 *
 * This class is an intuitive layer over the API offered by the
 * i3wm IPC interface.
 */
class QI3PCSHARED_EXPORT qi3pc : public QObject
{
  Q_OBJECT

public:
  // magic string used by the current API
  static constexpr auto IpcMagicString = I3_IPC_MAGIC;
  static constexpr auto IpcMagicLength = 6;

  /*!
   * \brief Enum holding the types of events offered by the
   * i3wm's IPC API.
   */
  enum class IpcEvent : quint32
  {
    Workspace = I3_IPC_EVENT_WORKSPACE,
    Output = I3_IPC_EVENT_OUTPUT,
    Mode = I3_IPC_EVENT_MODE,
    Window = I3_IPC_EVENT_WINDOW,
    BarUpdate = I3_IPC_EVENT_BARCONFIG_UPDATE,
    Binding = I3_IPC_EVENT_BINDING,
    Shutdown = I3_IPC_EVENT_SHUTDOWN,
    Tick = I3_IPC_EVENT_TICK
  };

  /*!
   * \brief Enum holding the types of messages the API expect to
   * send and receive replies for from i3.
   */
  enum class IpcType : quint32
  {
    Command = I3_IPC_REPLY_TYPE_COMMAND,
    Workspaces = I3_IPC_REPLY_TYPE_WORKSPACES,
    Subscribe = I3_IPC_REPLY_TYPE_SUBSCRIBE,
    Outputs = I3_IPC_REPLY_TYPE_OUTPUTS,
    Tree = I3_IPC_REPLY_TYPE_TREE,
    Marks = I3_IPC_REPLY_TYPE_MARKS,
    BarConfig = I3_IPC_REPLY_TYPE_BAR_CONFIG,
    Version = I3_IPC_REPLY_TYPE_VERSION,
    BindingModes = I3_IPC_REPLY_TYPE_BINDING_MODES,
    Config = I3_IPC_REPLY_TYPE_CONFIG,
    Tick = I3_IPC_REPLY_TYPE_TICK,
    Sync = I3_IPC_REPLY_TYPE_SYNC
  };

  /*!
   * \brief Enum holding the types of change a workspace event
   * can have.
   *
   * This information is taken from the change (a string) received
   * with the event.
   */
  enum class WorkspaceChange
  {
    Focus,
    Init,
    Empty,
    Urgent,
    Reload,
    Rename,
    Restored,
    Move,
    Unknown
  };

  /*!
   * \enum OutputChange
   * \brief Enum holding the types of change an output event
   * can have.
   *
   * This information is taken from the change (a string) received
   * with the event.
   */
  enum class OutputChange
  {
    Unspecified, /*!< see i3 docs. */
    Unknown      /*!< Unsupported change. */
  };

  /*!
   * \enum WorkspaceChange
   * \brief Enum holding the types of change a window event
   * can have.
   *
   * This information is taken from the change (a string) received
   * with the event.
   */
  enum class WindowChange
  {
    New,        /*!< A new window is created. */
    Close,      /*!< A window is closed. */
    Focus,      /*!< A window gained focus. */
    Title,      /*!< A window's title changed. */
    Fullscreen, /*!< A window as entered/leave fullscreen mode. */
    Move,       /*!< A window have been moved. */
    Floating,   /*! A window has entered/leaved floating mode. */
    Urgent,     /*! A window gained/lost urgent status. */
    Mark,       /*! A mark have been added/remvoed from a window. */
    Unknown     /*!< Unsupported change. */
  };

  /*!
   * \enum ShutdownChange
   * \brief Enum holding the types of change a shutdown event
   * can have.
   *
   * This information is taken from the change (a string) received
   * with the event.
   */
  enum class ShutdownChange
  {
    Restart, /*!< The window mananger is restarted. */
    Exit,    /*!< The window manager is exiting. */
    Unknown  /*!< Unsupported change. */
  };

  /*!
   * \enum WorkspaceChange
   * \brief Enum holding the types of change a binding event
   * can have.
   *
   * This information is taken from the change (a string) received
   * with the event.
   */
  enum class BindingChange
  {
    Run,    /*!< A binding was run. */
    Unknown /*!< Unsupported change. */
  };

  /*!
   * Alias used for cached JSON objects.
   */
  using DataObject = std::optional<std::pair<QJsonObject, qint64>>;

  /*!
   * Alias used for cached JSON arrays.
   */
  using DataArray = std::optional<std::pair<QJsonArray, qint64>>;
  using Error = std::optional<QString>;
  using Message = std::optional<std::pair<QJsonDocument, quint32>>;

  /*!
   * \brief Construct an qi3pc object.
   * \param parent Parent of the qi3pc object to create
   */
  explicit qi3pc(QObject* parent = nullptr);

  /*!
   * \brief Simple destructor for the qi3pc class.
   *
   * Disconnects the used sockets.
   * \see qi3pc::disconnect
   */
  virtual ~qi3pc();

  /*!
   * \brief Connect to the unix socket.
   * \return The success of the operation
   *
   * This method may wait up to 3 seconds for the connection to be
   * established. If it takes more than 3 seconds to connect, the
   * connection might be established later, but the returned value
   * will not reflect that.
   *
   * \see qi3pc::connected
   */
  bool connect();

  /*!
   * \brief Check if the connection to the ipc socket is established.
   * \return Return if the connection to the two QLocalSocket object's
   * used are connected.
   *
   * \see qi3pc::connected
   */
  bool connected();

  /*!
   * \brief Disconnect from the ipc
   * \return Return if the socket connection have been closed.
   *
   * \see qi3pc::connected
   */
  bool disconnect();

  /*!
   * \brief Subscribe to a list of events.
   * \param events A list of string with the
   * <a href="https://i3wm.org/docs/ipc.html#_available_events">events</a>
   *  to subscribe to.
   */
  void subscribe(const QStringList& events);

  /*!
   * \brief Get the path to the i3 ipc local unix socket.
   * \return The path to the unix socket
   */
  QString socketPath() const;

  /*!
   * \brief Send a message with the specified type and payload to i3.
   * \param type The type of the message to send.
   * \param payload The content of the message.
   *
   * Available <a
   * href="https://i3wm.org/docs/ipc.html#_sending_messages_to_i3">message
   * types</a>.
   *
   * Do not use this method to send \link qi3pc::IpcType::Subscribe
   * messages, use \link qi3pc::subscribe instead.
   */
  void sendMessage(IpcType type, const QByteArray& payload = QByteArray());

  /*!
   * \brief Get the list of (cached) worksaces.
   * \return The list of workspaces with their last update time.
   *
   * \see qi3pc::fetchWorkspaces
   */
  DataArray workspaces() const;

  /*!
   * \brief Get the (cached) i3 layout tree.
   * \return The layout tree with their last update time
   *
   * \see qi3pc::fetchTree
   */
  DataObject tree() const;

  /*!
   * \brief Get the (cached) list of outputs.
   * \return The list of outputs with their last update time.
   *
   * \see qi3pc::fetchOutputs
   */
  DataArray outputs() const;

  /*!
   * \brief Get the (cached) list of set marks.
   * \return The list of marks and their last update time.
   *
   * \see qi3pc::fetchMarks
   */
  DataArray marks() const;

  /*!
   * \brief Get the (cached) list of all bar configurations.
   * \return A map of bar configuration and their last update time.
   *
   * \see qi3pc::fetchBarConfig
   * \see qi3pc::barConfigUpdated
   * \see qi3pc::newBarConfig
   */
  DataObject barConfigs() const;

  /*!
   * \brief Get the (cached) i3 version object.
   * \return The i3 version and the last time it was updated.
   *
   * \see qi3pc::fetchVersion
   * \see qi3pc::versionUpdated
   */
  DataObject version() const;

  /*!
   * \brief Get the (cached) list of binding modes.
   * \return The list of binding modes and their last update time.
   * \see qi3pc::fetchModes
   * \see qi3pc::modesUpdated
   */
  DataArray modes() const;

  /*!
   * \brief Get the (cached) data read from the config file.
   * \return The config and the last time it was updated.
   * \see qi3pc::fetchConfig
   * \see qi3pc::configUpdated
   */
  DataObject config() const;

private:
  /*!
   * \brief Read one message using the socket parameter.
   * \param socket Local unix socket from which the message is read
   * \return An optional json doc and the type of the message read
   */
  Message processMessage(QLocalSocket& socket);

  /*!
   * \brief Read data from the event socket.
   */
  void processEvent();

  /*!
   * \brief Read data from the message socket.
   */
  void processReply();

  /*!
   * \brief Handle data received from a workspace event.
   * \param doc Document containing the data
   */
  void processWorkspaceEvent(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from an output event.
   * \param doc Document containing the data
   */
  void processOutputEvent(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a mode event.
   * \param doc Document containing the data
   */
  void processModeEvent(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a window event.
   * \param doc Document containing the data
   */
  void processWindowEvent(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from an update event.
   * \param doc Document containing the data
   */
  void processBarUpdateEvent(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a binding event.
   * \param doc Document containing the data
   */
  void processBindingEvent(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a shutdowm event.
   * \param doc Document containing the data
   */
  void processShutdownEvent(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a tick event.
   * \param doc Document containing the data
   */
  void processTickEvent(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a run command reply.
   * \param doc Document containing the data
   */
  void processCommandReply(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a workspace reply.
   * \param doc Document containing the data
   */
  void processWorkspaceReply(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from an output reply.
   * \param doc Document containing the data
   */
  void processOutputReply(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a tree reply.
   * \param doc Document containing the data
   */
  void processTreeReply(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a mark reply.
   * \param doc Document containing the data
   */
  void processMarkReply(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a bar config reply.
   * \param doc Document containing the data
   */
  void processBarConfigReply(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a version reply.
   * \param doc Document containing the data
   */
  void processVersionReply(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a mode reply.
   * \param doc Document containing the data
   */
  void processModeReply(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a config reply.
   * \param doc Document containing the data
   */
  void processConfigReply(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a tick reply.
   * \param doc Document containing the data
   */
  void processTickReply(const QJsonDocument& doc);

  /*!
   * \brief Handle data received from a sync reply.
   * \param doc Document containing the data
   */
  void processSyncReply(const QJsonDocument& doc);

  /*!
   * \brief Convert a string into a workspace change object.
   * \param s The string to convert
   * \return A qi3pc::WorkspaceChange value.
   */
  WorkspaceChange workspaceChangeFromString(const QString& s) const;

  /*!
   * \brief Convert a string into a window change object.
   * \param s The string to convert
   * \return A qi3pc::WindowChange value.
   */
  WindowChange windowChangeFromString(const QString& s) const;

  /*!
   * \brief Convert a string into a shutdown change object.
   * \param s The string to convert
   * \return A qi3pc::ShutdownChange value.
   */
  ShutdownChange shutdownChangeFromString(const QString& s) const;

  /*!
   * \brief Convert a string into an output change object.
   * \param s The string to convert
   * \return A qi3pc::OutputChange value.
   */
  OutputChange outputChangeFromString(const QString& s) const;

  /*!
   * \brief Convert a string into a binding change object.
   * \param s The string to convert
   * \return A qi3pc::BindingChange value.
   */
  BindingChange bindingChangeFromString(const QString& s) const;

private:
  QString m_socketPath; /*!< Path of the local unix socket used. */

  QLocalSocket
    m_eventSocket; /*!< Socket used to subscribe and handle events. */
  QLocalSocket
    m_messageSocket; /*!< Socket used to send messages and handle replies. */

  DataObject m_tree;       /*!< Layout tree cache. */
  DataArray m_workspaces;  /*!< Workspace list cache. */
  DataArray m_outputs;     /*!< Output list cache. */
  DataArray m_marks;       /*!< Mark list cache. */
  DataObject m_barConfigs; /*!< Bar configuration list cache. */
  DataObject m_version;    /*!< Version cache. */
  DataArray m_modes;       /*!< Modes list cache. */
  DataObject m_config;     /*!< Configuration cache. */

signals:
  /*!
   * \brief Signal emitted when a command have been ran by i3
   * \param success Command exit status (??)
   * \param error Optional human readable string describing an error
   *
   * Use \link qi3pc::sendMessage to run command with
   * the \link qi3pc::IpcType::Command type.
   */
	void commandRan(bool success, qi3pc::Error error);

  /*!
   * \brief Signal emitted when a tick have been processed by i3.
   * \param success ??
   *
   * Use \link qi3pc::sendMessage to send tick messages with
   * the \link qi3pc::IpcType::Tick type.
   */
  void tickSent(bool success);

  /*!
   * \brief Signal emitted when a sync message have been replied
   * to by i3.
   * \param success ??
   *
   * Use \link qi3pc::sendMessage to send sync messages with
   * \the \link qi3pc::IpcType::Sync type.
   */
  void synced(bool success);

  /*!
   * \brief Signal emitted when a subscribe message have been
   * replied to.
   * \param success ??
   *
   * \see qi3pc::subscribe
   */
  void subscribed(bool success);

  /*!
   * \brief Signal emitted with a workspace event's data preprocessed.
   * \param change The type of change
   * \param current The current workspace
   * \param old The old workspace
   */
	void workspaceEvent(qi3pc::WorkspaceChange change,
                      const QJsonObject& current,
                      const QJsonObject& old);
  /*!
   * \brief Signal emitted when the output(s) change.
   * \param change The type of change
   */
	void outputEvent(qi3pc::OutputChange change);

  /*!
   * \brief Signal emitted when the binding mode changes.
   * \param change The name of the current mode.
   * \param pango Boolean telling whether to display the mode
   * with pango markup.
   */
  void modeEvent(QString change, bool pango);

  /*!
   * \brief Signal emitted when a window changes.
   * \param change The type of change.
   * \param container The parent of the changed window.
   */
	void windowEvent(qi3pc::WindowChange change, const QJsonObject& container);

  /*!
   * \brief Signal emitted when a bar's configuration have been
   * updated.
   * \param doc Json object containing the bar configuration.
   */
  void barUpdateEvent(const QJsonObject& doc);

  /*!
   * \brief Signal emitteed when a binding have been triggered to run
   * a command.
   * \param change The type of change.
   * \param binding The binding that was run.
   */
	void bindingEvent(qi3pc::BindingChange change, const QJsonObject& binding);

  /*!
   * \brief Signal emitted when the ipc socket i about to shutdown.
   * \param change The type of change.
   */
	void shutdownEvent(qi3pc::ShutdownChange change);

  /*!
   * \brief Signal emitted when subscribing to tick events or when a
   * tick message have been sent to the ipc connection.
   * \param first Boolean specifying whether this is a subscription
   * \param payload Arbitrary payload sent with the tick message.
   * Empty for subscriptions.
   */
  void tickEvent(bool first, const QJsonObject& payload);

  /*!
   * \brief Signal to emit to trigger an update of the list of
   * workspace cache.
   * \see qi3pc::workspaces
   * \see qi3pc::workspacesUpdated
   */
  void fetchWorkspaces();

  /*!
   * \brief Signal emitted when the (cached) list of workspaces
   * have been updated.
   * \param workspaces The last list of workspaces with the update
   * time.
   * \see qi3pc::fetchWorkspaces
   */
	void workspacesUpdated(const qi3pc::DataArray& workspaces);

  /*!
   * \brief Signal to emit to trigger an update of the (cached)
   * layout tree.
   * \see qi3pc::tree
   * \see qi3pc::treeUpdated
   */
  void fetchTree();

  /*!
   * \brief Signal emitted when the layout tree cache have been
   * updated.
   * \param tree The most recent layout tree cached and its
   *  update time.
   * \see qi3pc::fetchTree
   */
	void treeUpdated(const qi3pc::DataObject& tree);

  /*!
   * \brief Signal to emit to trigger an update of
   * the (cached) outputs.
   *
   * \see qi3pc::outputs
   * \see qi3pc::outputsUpdated
   */
  void fetchOutputs();

  /*!
   * \brief Signal emitted when (cached) outputs have been updated.
   * \param outputs The latest list of outputs and their update time.
   * \see qi3pc::fetchOutputs
   */
	void outputsUpdated(const qi3pc::DataArray& outputs);

  /*!
   * \brief Signal to emit to trigger an update of the (cached)
   * list of marks.
   * \see qi3pc::marks
   * \see qi3pc::marksUpdated
   */
  void fetchMarks();

  /*!
   * \brief Signal emitted when the (cached) list of marks have
   * been updated.
   * \param marks The most recent list of marks and the time
   * when it was updated.
   * \see qi3pc::fetchMarks
   */
	void marksUpdated(const qi3pc::DataArray& marks);

  /*!
   * \brief Signal to emit to update the (cached) configuration of a
   * certain bar.
   * \param id String identifying to bar to update.
   *
   * \see qi3pc::barConfigUpdated
   * \see qi3pc::newBarConfig
   * \see qi3pc::barConfigs
   */
  void fetchBarConfig(const QString& id);

  /*!
   * \brief Signal to emit to update the list of bar configurations.
   * \see qi3pc::barConfigUpdated
   * \see qi3pc::newBarConfig
   * \see qi3pc::barConfigs
   */
	void fetchBarConfigs();

  /*!
   * \brief Signal emitted when a specific bar's (cached) config have
   * been updated.
   * \param config The relevant bar's configuration.
   *
   * \see qi3pc::barConfigs
   */
  void barConfigUpdated(const QJsonObject& config);

  /*!
   * \brief Signal emitted when a new bar config have been added
   * to the cache.
   * \param id The id of the new bar.
   *
   * Only the id is stored at first. Call \link qi3pc::fetchBarConfig with
   * the appropriate id to update it.
   */
  void newBarConfig(const QString& id);

  /*!
   * \brief Signal to emit to trigger a cache update for the
   * i3wm version.
   *
   * \see qi3pc::version
   * \see qi3pc::versionUpdated
   */
  void fetchVersion();

  /*!
   * \brief Signal emitted when the (cached) i3 version
   * have been updated.
   * \param version Json object with the latest cached version and the
   * time when it was updated.
   *
   * \see qi3pc::version
   */
	void versionUpdated(const qi3pc::DataObject& version);

  /*!
   * \brief Signal to emit to trigger an update of the (cached)
   * list of modes.
   *
   * \see qi3pc::modes
   * \see qi3pc::modesUpdated
   */
  void fetchModes();

  /*!
   * \brief Signal emitted when the (cached) list of modes
   * have been updated.
   * \param modes The most recent list of modes and the time
   * when it was updated.
   *
   * \see qi3pc::modes
   */
	void modesUpdated(const qi3pc::DataArray& modes);

  /*!
   * \brief Signal to emit to trigger an update of the (cached) config.
   *
   * \see qi3pc::config
   * \see qi3pc::configUpdated
   */
  void fetchConfig();

  /*!
   * \brief Signal emitted when the (cached) config have been updated.
   * \param The most recent config and the time when it was updated.
   *
   * \see qi3pc::config
   */
	void configUpdated(const qi3pc::DataObject& config);
};

#endif // QI3PC_H
